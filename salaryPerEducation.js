/*
 * Programming Quiz: Back to School (3-9)
 */
// change the value of `education` to test your code
var education = "a high school diploma";
// set the value of this based on a person's education
var salary;
switch (education) {
    case "no high school diploma" :
        var salary = 25636;
        break;
    case "a high school diploma" :
        var salary = 35256;
        break;
    case "as Associate's degree" :
        var salary = 41496;
        break;
    case "a Bachelor's degree" :
        var salary = 59124;
        break;
    case "a Master's degree" :
        var salary = 69732;
        break;
    case "a Professional degree" :
        var salary = 89960;
        break;
    case "a Doctoral degree" :
        var salary = 84396;
        break;
}
console.log("In 2015, a person with "+education+" earned an average of $"+salary.toLocaleString("en-US")+"/year.");