/*
 * Programming Quiz: Inline Functions (5-6)
 */

// don't change this code
function emotions(myString, myFunc) {
    console.log("I am " + myString + ", " + myFunc(2));
}
var laugh = function(num){
    var statement="ha";
    console.log(statement.repeat.num());
};
// your code goes here
// call the emotions function here and pass in an
// inline function expression
emotions("happy", laugh(2));