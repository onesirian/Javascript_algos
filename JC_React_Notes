Resources

***General Documentation***
https://reactjs.org/
https://github.com/facebook/create-react-app
https://reactstrap.github.io/

***Beginner's Guide***
https://egghead.io/courses/the-beginner-s-guide-to-react 
youtube video "React Introduction" by Maximilian Schwarzuller

***Passing Props***
https://medium.com/@ruthmpardee/passing-data-between-react-components-103ad82ebd17

*** Awesome React Event Handling Tips***
https://appendto.com/2017/01/react-events-101/

***Importing and exporting Javascript methods***
https://hackernoon.com/import-export-default-require-commandjs-javascript-nodejs-es6-vs-cheatsheet-different-tutorial-example-5a321738b50f


Github React Projects for References
https://github.com/jcsmileyjr/active-cases  ***Best and Cleanest***
https://github.com/jcsmileyjr/Study-Buddy



Notes

***To start an CRA app: ****
1. npx create-react-app "name of the app"
2. npm install react-bootstrap
3. npm install bootstrap@3
4. put "import 'bootstrap/dist/css/bootstrap.min.css' at the top of the index.jsfile
5. Add project to Github
6. cd my-app
7. npm start

***Difference in a React Class Component and Functions***
class NameOfClass extends React.Component {
  constructor(props){
  super(props);
  this.state = {};	//data that can be change later
  }
  
  render() {
    return (
     //html code to be displayed
    );
   }
}

  //method to use inside the class and pass down to children
  changeName(){
	this.setState({"object data that can be change"});
  }

  //this is not a class and use the props as a properties link to attributes
  function NameofClass(props) {
    return (
      <div>
	<h1>{props.NameOfAttribute}</h1>
	<Cases />	//Display a Component
	<NumberList />	//Display a Component
      </div>
     );
}

***how to display an array of stuff***
function NumberList(props){
  const numbers= [1,2,3,4,5];
  const listOfNumbers = numbers.map((number) =>
    <li>{number}</li>);		
											  
      return(
	<ul>{listOfNumbers}</ul>									 
      );
}


***Images and Pictures***
In create-react-app, relative paths for images don't seem to work. Instead, you can import an image.
- Example from OurHeritage Project
import homePagePic from '../Images/bigMomma1-compressor.jpg';

<img className="img-responsive center-block" src={homePagePic} alt="Older african american female" />

***JavaScript ES6 arrow function without body and implicit return***
const getGreeting = () => 'Welcome to JavaScript';

***JavaScript ES6 funtional stateless component***
const Greeting = (props) => <h1>{props.greeting}</h1>

***Example of using ES6 to auto-bind a method (onDecrement()) to the class's constructor and use in the JSX as a event method. ***
class Counter extends Component {
  state = {counter: 0};

  onDecrement = () => {
    this.setState(state => ({ counter: state.counter - 1 }));
  }
  
  render(){<button onClick={this.onDecrement} type="button">Increment</button>}

***importing and exporting functions Example***
/*calculate.js*/
- export function add(x,y){return x + y};
- export function multiply(x, y){return x * y};

/*main.js*/
- import {add} from 'calculate';
- import {multiply} from 'calculate';
or 
- import * as main from 'main';
main.add(2,2);
main.multiply(2,2);

***	Tips for Styling	****
- https://codeburst.io/4-four-ways-to-style-react-components-ac6f323da822
example: 
	const appBackground={backgroundColor:"purple"};

	class App extends React.Component{
	  render(){
		return(
		  <div style={appBackground}>
			Hello Sunshine!!!
		  </div>
		);	  
	  }
	}

*** import like this*** 
- import Case from './Components/Case/Case.js'

***import react-bootstrap components***
import { Navbar, Jumbotron, Button } from 'react-bootstrap';

***Pure Function component using props***		
function Title(props) {
   return (
    <div>
     <h1>{props.appTitle}</h1>
     <Cases />
     <NumberList />	//display an array of numbers
    </div>
   );
}

***Example of Exporting Components***
class DangerButton extends Component {
  render() {
    return <Button color="red" />;
  }
}

export default DangerButton;

***Example of using ES6 Functions
const HelloWorld = ({name}) => (
 <div>{`Hi ${name}`}</div>
);

/*  OR  */
const HelloWorld = ({name}) => <div>{`Hi ${name}`}</div>;

/*  OR  */
function HelloWorld ({name}) {
  return (<div> Hi {name} </div> )
}

***Problem: data haven't loaded before View is loaded	***
const VideoDetail = ({video}) => {
  if(!video){	
  	return <div>Loading ....</div>	//Load this if video is null (havn't download) yet
  }
  
  return(
  	<div>NORMAL STUFF</div>
  );
}

***Increment a counter in React	****
- tip from https://stackoverflow.com/questions/39316376/how-to-use-the-increment-operator-in-react
this.setState((prevState, props) => ({
    counter: prevState.counter + 1
})); 


***Passing data up from child to Parent	***
* use both below to help understand
- tip from https://codepen.io/PiotrBerebecki/pen/dpRdKP?editors=0010
- https://reactjs.org/docs/lifting-state-up.html

//below is from the active-cases project
class InputCase extends Component{
  constructor(props){
	  super(props);
	  this.onInputChange = this.onInputChange.bind(this);//bind the method
  }
  render(){
	  return(
		<div className="row selectContainer">
		  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 textColor">
		  	What is the patron name?
		  </div>
		  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<input type="text" onChange={this.onInputChange} //onChange is an event handerler. It calls the method
		  </div>	  
		</div>  
  );	  
  }

  onInputChange(event){
	  event.preventDefault(); //not sure
	  
	  this.props.updatePatron(event.target.value);
	  //IMPORTANT. Use a callback function pass DOWN from parent and return the data (event.target.value) to the parent. 
  }	
}

//On the parent Component
<NewCase updatePatron={this.callBack Method Name} />

//if mulitple levels deep. The second tier component will use props
<InputCase updatePatron={this.props.updatePatron} />

****display alternating class ***
 
const caseLoad = cases.map((files, index) =>
	<li className={index % 2 ===0 ? "mainColor":"testColor"} key={files.caseNumber}><Case type={files.type} casino={files.casino} patron={files.patron} status={files.status} /></li>					 
   );
  return caseLoad;

*******************	Example of making and using functions	**************
	//This creates a class component that encapsulate all data for its ui
	//Components should be upperCase
	class Cases extends React.Component {
			constructor(props){	//setup the attributes use by the UI
				super(props);	//must have
				this.state = {startDate:"January 1, 2018", type:"dispute", casino:"Horseshoe", patronName:"Bob Doyle", status:"waiting on letter"}; //use a object
				
				//IMPORTANT: needed for functions call inside the component
				this.frenchify = this.frenchify.bind(this);
			}//end of constructor
			
			//this is run when the component is initialize in the DOM
			componentDidMount(){
				this.changeName();
			}
			
			//method use in the component. 
			//this.setState must an object and can change the state information
			changeName(){
				this.setState({patronName:'Bill Clinton'});
			}
			
			//method use with a BUTTON to change a state
			frenchify() {
			  this.setState({ casino: 'French Horshoe' });
			}
			
			render() {
				return (
					<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<ul>
						<li>{this.state.startDate}</li>//	must state this.state.#$%
						<li>{this.state.type}</li>
						<li>{this.state.casino}</li>
						<li>{this.state.patronName}</li>
						<li>{this.state.status}</li>
					</ul>
					<button onClick={this.frenchify}>Frenchify!</button> 
					</div>

				);
			}
		}

***Example of Switching Between Components***
function VisitorMessage(props) {
  let loggedIn = true; // change this to false
  return (
    <div>
      {loggedIn && <UserGreeting />}
      {!loggedIn && <GuestGreeting />}
    </div>
  );
***Example of Input Component***
class InputCase extends Component{
  constructor(props){
	  super(props);
	  this.state = {patronName:""};	//data for this component
	  this.onInputChange = this.onInputChange.bind(this);//bind the method
  }
  render(){
	  return(
		<div className="row selectContainer">
		  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 textColor">
		  	What is the patron name?
		  </div>
		  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<input type="text" onChange={this.onInputChange} />
		  </div>	  
		</div>  
  );	  
  }
}
