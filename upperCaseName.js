/*This code snipped takes two names such as
	James Jones, then automatically makes sure
	the first letter of the first name is 
	capitalized, and all letters of the second
	namd are capitalized*/

function nameChanger(oldName) {
	var finalName;
	var names = oldName.split(" ");
	names[1]= names[1].toUpperCase();
	names[0]=names[0].slice(0,1).toUpperCase() + names[0].slice(1).toLowerCase();
	finalName = names.join(" ");
	return finalName;	
}
