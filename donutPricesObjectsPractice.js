/*
 * Programming Quiz: Donuts Revisited (7-6)
 */

var donuts = [
    { type: "Jelly", cost: 1.22 },
    { type: "Chocolate", cost: 2.45 },
    { type: "Cider", cost: 1.59 },
    { type: "Boston Cream", cost: 5.99 }
];
donuts.forEach(function(arrayitem){
    console.log(arrayitem.type+" donuts cost $"+arrayitem.cost+" each");
});
/*
 * Personal note for me here. When we use the forEach method, we've got to 
 * put that parameter in there and then utilize that in the function iself"
 */
 