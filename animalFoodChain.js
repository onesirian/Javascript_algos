/*
 * Programming Quiz - Navigating the Food Chain (3-8)
 */

// change the values of `eatsPlants` and `eatsAnimals` to test your code
var eatsPlants = false;
var eatsAnimals = false;
var category;
if (eatsPlants && eatsAnimals) {
        var category = "omnivore";
} else if (eatsPlants || eatsAnimals) {
    var category = (eatsPlants) ? "herbivore" : "carnivore";
} else {
    var category = "undefined";
}
console.log(category);
